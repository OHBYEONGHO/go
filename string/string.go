package main

import (
	"fmt"
	"reflect"
	"strings"
	"unsafe"
)

func main() {
	var str1 string = "Hello world"
	var str2 string = str1

	stringHeader1 := (*reflect.StringHeader)(unsafe.Pointer(&str1))
	stringHeader2 := (*reflect.StringHeader)(unsafe.Pointer(&str2))

	fmt.Println("=======================")
	fmt.Println(stringHeader1)
	fmt.Println(stringHeader2)
	fmt.Println("=======================")

	var str3 string = "hello world3"
	var slice []byte = []byte(str3)

	stringHeader3 := (*reflect.StringHeader)(unsafe.Pointer(&str3))
	sliceHeader := (*reflect.StringHeader)(unsafe.Pointer(&slice))

	fmt.Println("=======================")
	fmt.Printf("str3:\t%x\n", stringHeader3.Data)
	fmt.Printf("slice:\t%x\n", sliceHeader.Data)
	fmt.Println("=======================")

	var str4 string = "Hello world4"
	fmt.Println("=======================")
	fmt.Println(ToUpper1(str4))
	fmt.Println(ToUpper2(str4))
	fmt.Println("=======================")
}

// strings.Toupper
func ToUpper1(str string) string {
	var rst string
	for _, c := range str {
		if c >= 'a' && c <= 'z' {
			rst += string('A' + (c - 'a'))
		} else {
			rst += string(c)
		}
	}
	return rst
}

func ToUpper2(str string) string {
	var builder strings.Builder
	for _, c := range str {
		if c >= 'a' && c <= 'z' {
			builder.WriteRune('A' + (c - 'a'))
		} else {
			builder.WriteRune(c)
		}
	}
	return builder.String()
}
