package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

var stdin = bufio.NewReader(os.Stdin)

func main() {
	rand.Seed(time.Now().UnixNano())

	// 위 seed를 메소드 대체 가능
	// time.Since(time.Now())

	r := rand.Intn(100)
	cnt := 1
	for {
		fmt.Println("숫자를 입력하시오: ")
		n, err := InputIntValue()
		if err != nil {
			fmt.Println("숫자만 입력하시오.")
		} else {
			if n > r {
				fmt.Println("입력하신 숫자가 더 큽니다.")
			} else if n < r {
				fmt.Println("입력하신 숫자가 더 작습니다.")
			} else {
				fmt.Println("정답. 시도한 횟수: ", cnt)
				break
			}
			cnt++
		}
	}
}

func InputIntValue() (int, error) {
	var n int
	_, err := fmt.Scanln(&n)
	if err != nil {
		stdin.ReadString('\n')
	}
	return n, err
}
