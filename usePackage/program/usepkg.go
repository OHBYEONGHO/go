package main

// go mod init - go mod tidy(import한 패키지 다운로드) - go.sum 패키지 버전 확인
// go env(go 환경설정 정보) - gopath 이동하여 다운로드된 패키지 물리파일 확인
import (
	"fmt"
	"go/usePackage/custompkg"
	"go/usePackage/exinit"

	"github.com/guptarohit/asciigraph"
	"github.com/tuckersGo/musthaveGo/ch16/expkg"
)

func main() {
	makeGraph()
}

func importTest() {
	custompkg.PrintCustom()
	// custompkg.printCustom2()
	expkg.PrintSample()

	s := custompkg.Student{"오병호", 33, 32}
	fmt.Println(s.Name, s.Age)
	custompkg.Ratio = 10
	fmt.Println(custompkg.Ratio)
}

func initPackage() {
	exinit.PrintD()
}

func makeGraph() {
	data := []float64{3, 4, 5, 6, 9, 1, 8, 2, 10, 5, 7, 3, 10}
	graph := asciigraph.Plot(data)
	fmt.Println(graph)
}
