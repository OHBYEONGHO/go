package custompkg

import "fmt"

type Student struct {
	Name  string
	Age   int
	Score int
}

var Ratio int

const PI = 3.14
const pI2 = 3.14529

func PrintCustom() {
	fmt.Println("This is custom Package")
}

func printCustom2() {
	fmt.Println("This is custom Package2")
}
